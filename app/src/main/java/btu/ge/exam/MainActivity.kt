package btu.ge.exam

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import java.math.RoundingMode

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }


    private fun init() {

        btnCalc.setOnClickListener {
            converter()
        }

    }


    private fun converter() {


        if (userInputcm.text.toString() == "25") {
            Toast.makeText(this, "გივი!? შენ ხარ გივი ??", Toast.LENGTH_LONG).show()
            BackgroundImage.setImageResource(R.drawable.givi)
        }
        if (userInputcm.text.isNotEmpty() && userInputinch.text.isNotEmpty()) {
            Toast.makeText(this, "შეიყვანეთ ან სანტიმეტრი ან ინჩი", Toast.LENGTH_LONG).show()
        } else if (userInputcm.text.isNotEmpty()) {
            val x = userInputcm.text.toString().toFloat()
            val calc = x / 2.54
            val answer = calc.toBigDecimal().setScale(2, RoundingMode.HALF_EVEN).toDouble()
            result.text = answer.toString() + "  inch"

        } else if (userInputinch.text.isNotEmpty()) {
            val x = userInputinch.text.toString().toFloat()
            val calc = x * 2.54
            val answer = calc.toBigDecimal().setScale(2, RoundingMode.HALF_EVEN).toDouble()
            result.text = answer.toString() + "  cm"
        } else {
            Toast.makeText(this, "შეავსეთ მინიმუმ ერთი ველი", Toast.LENGTH_LONG).show()
        }
    }


}
